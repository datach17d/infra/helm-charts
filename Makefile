
BASE_DIR := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
CHART_DIRS := $(shell find $(BASE_DIR) -type f -name Chart.yaml -exec dirname {} \;)
PACKAGE_FILES := $(foreach dir,$(CHART_DIRS),$(dir)/$(shell helm show chart $(dir) | grep -m1 '^name:' | awk '{print $$2}')-$(shell helm show chart $(dir) | grep -m1 '^version:' | awk '{print $$2}').tgz)

.PHONY: all clean

all: $(PACKAGE_FILES)

upload: $(addsuffix .upload,$(PACKAGE_FILES))

%.lint:
	@echo "Linting Helm chart in $(@D)"
	helm lint $(@D) | tee $@

%.tgz: %.lint
	@echo "Packaging Helm chart in $(@D)"
	helm package --debug --dependency-update --destination $(@D) $(@D)

%.tgz.upload: %.tgz
	@echo "Uploading Helm chart in $(@D)"
	curl --request POST --user gitlab-ci-token:${CI_JOB_TOKEN} --form "chart=@$^" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/${CI_PROJECT_NAME}/charts"

clean:
	rm -f */*.lint
	rm -f $(PACKAGE_FILES)


