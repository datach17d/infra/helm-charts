{{/*
Expand the name of the chart.
*/}}
{{- define "metal3-cluster.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "metal3-cluster.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "metal3-cluster.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "metal3-cluster.labels" -}}
helm.sh/chart: {{ include "metal3-cluster.chart" . }}
{{ include "metal3-cluster.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "metal3-cluster.selectorLabels" -}}
app.kubernetes.io/name: {{ include "metal3-cluster.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "metal3-cluster.hashedname" -}}
{{- printf "%s-%s" (include "metal3-cluster.fullname" .root) (.data | toYaml | sha256sum) | trunc 63 }}
{{- end }}

{{- define "metal3-cluster.registration" -}}
criSocket: {{ .common.criSocket }}
name: '{{ "{{" }} ds.meta_data.name {{ "}}" }}'
kubeletExtraArgs:
  node-labels: 'metal3.io/uuid={{ "{{" }} ds.meta_data.uuid {{ "}}" }}'
  cgroup-driver: {{ .common.cgroupDriver }}
{{- if .custom.taints }}
{{- if eq (toString .custom.taints) "[]" }}
taints: []
{{- else }}
taints:
  {{- .custom.taints | toYaml | nindent 2 }}
{{- end }}
{{- end }}
{{- end }}


{{- define "metal3-cluster.joinConfiguration" -}}
joinConfiguration:
  controlPlane: {}
  nodeRegistration:
    {{- include "metal3-cluster.nodeRegistration" . | nindent 2 }}
{{- end }}

{{- define "metal3-cluster.KubeadmControlPlane.name" -}}
{{- include "metal3-cluster.Metal3MachineTemplate.controlplane.name" . }}
{{- end }}

{{- define "metal3-cluster.KubeadmConfigTemplate.name" -}}
{{- include "metal3-cluster.hashedname" (dict "data" .Values.common.kubeadm "root" .) }}
{{- end }}



{{- define "metal3-cluster.KubeadmControlPlane.initjoinConfiguration" -}}
initConfiguration:
  nodeRegistration:
    {{- include "metal3-cluster.registration" (dict "common" .Values.common.kubeadm "custom" .Values.controlplane) | nindent 4 }}
joinConfiguration:
  controlPlane: {}
  nodeRegistration:
    {{- include "metal3-cluster.registration" (dict "common" .Values.common.kubeadm "custom" .Values.controlplane) | nindent 4 }}
{{- end }}

{{- define "metal3-cluster.KubeadmConfigTemplate.joinConfiguration" -}}
joinConfiguration:
  nodeRegistration:
   {{- include "metal3-cluster.registration" (dict "common" .Values.common.kubeadm "custom" .Values.workers) | nindent 4 }}
{{- end }}


{{- define "metal3-cluster.Metal3Cluster.name" -}}
{{- include "metal3-cluster.hashedname" (dict "data" .Values.apiserver "root" .) }}
{{- end }}

{{- define "metal3-cluster.MachineDeployment.name" -}}
{{- include "metal3-cluster.fullname" . }}
{{- end }}


{{- define "metal3-cluster.Metal3MachineTemplate.controlplane.name" -}}
{{- if not .Values.controlplane.virtual -}}
{{- printf "controlplane-%s" ( include "metal3-cluster.hashedname" (dict "data" (dict "common" .Values.common.machineTemplate "custom" .Values.controlplane) "root" .) ) | trunc 63 }}
{{- else -}}
{{- printf "controlplane-%s" ( include "metal3-cluster.fullname" . ) }}
{{- end -}}
{{- end }}

{{- define "metal3-cluster.Metal3MachineTemplate.worker.name" -}}
{{- printf "worker-%s" ( include "metal3-cluster.hashedname" (dict "data" (dict "common" .Values.common.machineTemplate "cusomt" .Values.workers) "root" .) ) | trunc 63 }}
{{- end }}


