{{/*
Expand the name of the chart.
*/}}
{{- define "flux-helm-releases.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "flux-helm-releases.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "flux-helm-releases.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "flux-helm-releases.labels" -}}
helm.sh/chart: {{ include "flux-helm-releases.chart" . }}
{{ include "flux-helm-releases.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "flux-helm-releases.selectorLabels" -}}
app.kubernetes.io/name: {{ include "flux-helm-releases.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "flux-helm-releases.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "flux-helm-releases.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "flux-helm-releases.ReleaseName" }}
{{- . | replace " " "-" | trunc 63 }}
{{- end }}

{{- define "flux-helm-releases.HelmReleaseName" }}
{{- printf "%s-%s" (include "flux-helm-releases.ReleaseName" .HelmReleaseName) (include "flux-helm-releases.fullname" .root) | replace " " "-" | trunc 63 }}
{{- end }}
